system('rake', 'mo') or fail

require 'gettext'
GetText.bindtextdomain "minidebconf", path: 'locale'

def current_edition
  @config[:current_edition].to_i
end

def language
  item.path =~ %r{^/#{get_year}/en/} && :en || :'pt-br'
end

def metadata(key)
  year = get_year
  @metadata ||= {}
  @metadata[year] ||= {}
  input = { year: year }.update(config[:metadata])
  @metadata[year][language] ||= @config[:metadata][language].dup.tap do |m|
    m.keys.each do |k|
      m[k] = m[k] % input
    end
  end
  @metadata[year][language][key]
end

def l_(link, target_language=nil)
  target_language ||= language
  if target_language == :en
    '/%d/en%s' % [get_year, link]
  else
    '/%d%s' % [get_year, link]
  end
end

def _(text)
  GetText.set_locale language
  GetText._(text)
end
alias :t :_

def change_language
  link = item.path.gsub(%r{^/[0-9]+(/en)?/}, '/')
  if language == :en
    link_to('<span class="fa fa-arrow-circle-down"></span> Português', l_(link, :'pt-br'))
  else
    link_to('<span class="fa fa-globe"></span> English', l_(link, :en))
  end
end
