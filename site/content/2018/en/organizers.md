---
title: Organizers
---

# Organizers

MiniDebConf Curitiba 2018 is organized by the volunteers below:

* Adriana Cássia da Costa
* Alisson Coelho
* Angelo Rosa
* Antonio C. C. Marques
* Antonio Terceiro
* Cleber Ianes
* Daniel Lenharo de Souza
* Gilmar Nascimento
* Giovani Ferreira
* Leonardo Rodrigues
* Lucas Kanashiro
* Paulo Henrique de Lima Santana
* Samuel Henrique
* Valéssio Brito

And the professors from UTFPR below:

* Prof. Adolfo Gustavo Serra Seca Neto (Departamento Acadêmico de Informática)
* Profa. Maria Claudia F. Pereira Emer (Departamento Acadêmico de Informática)