---
title: Volunteers
---

# Volunteers

Since MiniDebConf Curitiba is an event organized by vonluteers, its quality
strongly depends on our effort and as such every help is appreciated. If you
are wondering how and with what exactly you can help, here is the list of some
of our teams which could always use a hand:

 - **Photography**: Bring your camera and take photos of activities

 - **Content**: During the event, post content in social medias/networks on
 behalf of Debian Brasil and Curitiba Livre communities (you will have access
 to these profiles)

 - **Secretary**: Help us to welcome and register the presence of the attendees
 during the event

 - **Talks streaming**: Support the talks recording/streaming. Do not worry!
 We will be there to help you :)

 - **Auditorium**: Help speakers during the event. Handle issues with
 microphones and projectors, time the talks, and whatever is needed.

<br>
<span style="font-size: 20px">Are you interested?! Do you want to help us?!
Register yourself in this
[form](http://pesquisa.softwarelivre.org/index.php/992958?lang=pt-BR) and we
will contact you!</span>
