---
title: Confirmed speakers
---

# Confirmed speakers

<!-- ALPHABETICAL ORDER (BY FIRST NAME) -->

## Adriana Cássia da Costa

![Adriana Cássia da Costa](/2018/images/fotos/adriana-costa.jpg "Adriana Cássia da Costa")

Graduada em Publicidade e Propaganda (UFMT) e Desenvolvimento de Sistemas para
Internet (IFMT), Mestra em Ciência da Computação (PUCRS). Atualmente atuo como
Analista de Sistemas, sou membro da comunidade Curitiba Livre e colaboro com o
Software Livre auxiliando na organização de eventos da área. Mantenho o blog
Mulheres, Tecnologia e Oportunidades que incentiva a participação de mulheres
na área de TI <http://mulheres.eti.br>.

------------------------------------------------------------------------

## Antonio Carlos C Marques

![Antonio Carlos C Marques](/2018/images/fotos/antonio-marques.jpg "Antonio Carlos C Marques")

Mestre em educação, professor de História e Filosofia, usuário GNU/Linux/Debian.

------------------------------------------------------------------------

## Daniel Lenharo de Souza

![Daniel Lenharo de Souza](/2018/images/fotos/daniel-lenharo.jpg "Daniel Lenharo de Souza")

Debian Developer (DD), analista de informática, especialista em redes e
segurança da informação. Membro das Comunidades Curitiba Livre e Debian,
promovendo o uso do Software Livre em diversos eventos pelo Brasil. Tem
auxiliado na organização de diversos eventos (FISL, MiniDebConf, SFD, FLISOL,
entre outros).

------------------------------------------------------------------------

## Helen Koike

![Helen Koike](/2018/images/fotos/helen-koike.jpg "Helen Koike")

Helen is graduated in Computer Science by the University of Campinas (Unicamp)
Brazil and Engineering by Telecom Paristech France and is currently working as
a core Engineer at Collabora within Collabora's kernel team. She contributes to
the effort to support Secure Boot in Debian, and was a speaker at Debconf17.

------------------------------------------------------------------------

## Holger Levsen

![Holger Levsen](/2018/images/fotos/holger-levsen.jpg "Holger Levsen")

Holger Levsen has been a Debian user for 20 years and started contributing 15
years ago. He got involved in doing QA work on Debian in 2007 via first working
on piuparts, which led him to start https://jenkins.debian.net in 2012. At the
end of 2013 he had the idea to use this jenkins setup and a small script to
build some packages twice and compare the results. That's how he got involved
in Reproducible Builds.

Besides working on technical issues of Debian he also greatly enjoys meeting
and interacting with people directly, so until recently he was also active in
organizing the Debian conferences. This made him start the DebConf videoteam,
which has resulted in a huge archive available online on
<https://video.debian.net> and today via git-annex as well. 

------------------------------------------------------------------------

## João Eriberto Mota Filho

![João Eriberto Mota Filho](/2018/images/fotos/joao-eriberto.jpg "João Eriberto Mota Filho")

Officer of the Brazilian Army. Networking and Networking Security Manager at
the Army. Professor at the Graduate Program in Digital Forensics at
Universidade Católica de Brasília (UCB). Author of the books _Linux & Seus
Servidores_ (Linux and Its Servers, 2000), _Pequenas Redes com Microsoft
Windows_ (Small Networks with Microsoft Windows, 2001), _Descobrindo o Linux_
(Discovering Linux, 3rd edition in 2012), and _Análise de tráfego em redes
TCP/IP_ (Traffic Analysis in TCP/IP networks, 2013). Debian Developer. Member
of the Debian Forensics team.

------------------------------------------------------------------------

## Jon "maddog" Hall

![Jon maddog Hall](/2018/images/fotos/jon-maddog.jpg "Jon maddog Hall")

Membro do Board of the Linux Professional Institute. Desde 1969, Maddog (como
ele prefere ser chamado) tem sido programador, designer e administrador de
sistemas, gerente de produto, gerente de marketing técnico, autor e educador e
atualmente trabalha como consultor. Sr. Hall se dedica ao sistema Unix desde
1980 e ao sistema Linux desde 1994, quando ele conheceu Linus Torvalds e
reconheceu a importância comercial do Linux,Free e Open Source Software. Desde
2006 Maddog vem trabalhando no Projeto Cauã, que criará trabalhos high-tech
permitindo estudantes de baixa renda a arcarem com uma universidade e
experienciarem um trabalho. Concluiu licenciatura em Engenharia e Comércio pela
Drexel University em 1973 e seu mestrado em RPI pela Troy, em 1977. Agora ele
também viaja o mundo palestrando sobre os benefícios do Open Source Software.

------------------------------------------------------------------------

## Jonas Smedegaard

![Jonas Smedegaard](/2018/images/fotos/jonas-smedegaard.jpg "Jonas Smedegaard")

Jonas Smedegaard is a freelance systems administrator and developer working
with resource sharing within and between organisations, mostly schools and
NGOs; designing systems both intuitive to use, strictly based on open standards,
and purely built from Free Software.

------------------------------------------------------------------------

## Miriam Retka

![Miriam Retka](/2018/images/fotos/miriam-retka.jpg "Miriam Retka")

Licenciada em Física pela UEPG. Usuária avançada de GNU/Linux há 2 anos.
Desenvolvedora web freelancer. Ministrou workshops de introdução ao Python e ao
editor Vim em 2017.

------------------------------------------------------------------------

## Paulo Roberto Alves de Oliveira (Kretcheu)

![Paulo Roberto Alves de Oliveira](/2018/images/fotos/paulo-kretcheu.jpg "Paulo Roberto Alves de Oliveira")

Administrador de redes a mais de 15 anos. Colaborador no Debian (DM). Falante
de Esperanto. Certificado LPI-C, UCP, Novell NCLA e DCTS. Pós-graduação
(Auditoria e segurança). Prof. Universitário. Engenheiro Mecânico. Apresentador
do Vídeo Blog: <http://kretcheu.com.br>

------------------------------------------------------------------------

## Renata D'Avila

![Renata D'Avila](/2018/images/fotos/renata-davila.png "Renata D'Avila")

Estagiária Outreachy com projeto Debian. Contribui para organizações como
PyLadies Porto Alegre e Django Girls.

------------------------------------------------------------------------

## Samuel Henrique

![Samuel Henrique](/2018/images/fotos/samuel-henrique.jpg "Samuel Henrique")

Técnico em Informática pela UTFPR, atualmente estudando Engenharia de
Computação na UTFPR e estagiando na MadeiraMadeira. Debian Maintainer atuando
principalmente no time Debian Security Tools Packaging Team , também contribuiu
com alguns pacotes no AUR, repositório comunitário do Arch Linux, além de
contribuir para projetos de software livre sempre que possível; aircrack-ng,
pixiewps, t50.
