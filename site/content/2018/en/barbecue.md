---
title: Barbecue
---

# Barbecue

On the Sunday, April 15hh, we will have a barbecue party for lunch at the
APUFPR - Associação dos Professores da UFPR (Association of UFPR Professors).

It will cost R$ 30,00 per person and payment must be done at the frontdesk
until the end of the conference. This amount gives you

* Food
* Non-alcoholic beverages (soda, juice)

Those who want alcoholic beverages can take their own. The venue has a fridge.

We will have options to vegetarians too.

APUFPR is located at Rua Dr. Alcides Vieira Arcoverde, 1193 - Jardim das
Américas - Curitiba. There is free parking available.

APUFPR is on the way to the airport, so if you are leaving on Sunday afternoon
leaving from there straight to the airport is more practical than going back
downtown first.
