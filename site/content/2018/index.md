---
title: Início
---

# MiniDebConf Curitiba 2018

Bem-vindo(a) ao site da MiniDebConf Curitiba 2018 que acontecerá de 11 a 14 de
abril no Campus Central da UTFPR - Universidade Tecnológica Federal do Paraná.
Veja [mais informações](about).

Você pode realizar a sua inscrição gratuitamente [aqui](registration/).

## Vídeos das palestras

Links pars os vídeos das palestras estão dispóniveis na
[página da programação](schedule).

## Fotos

Veja as fotos do evento em:

[Álbum no flickr](https://www.flickr.com/photos/curitibalivre/albums/72157690015019780)

<!--
## Chamada de voluntários(as)

Caso queira nos ajudar na organização do evento, dê uma olhada na nossa
[https://meetings-archive.debian.net/pub/debian-meetings/2018/mini-debconf-curitiba](https://meetings-archive.debian.net/pub/debian-meetings/2018/mini-debconf-curitiba/).

## Hospedagem solidária

Precisa de um local para se hospedar, ou quer oferecer hospedagem para alguém? 
Veja nossa página de [hospedagem solidária](couchsurfing).

Até o dia 31 de janeiro de 2018 você pode enviar uma proposta de atividade na
página da nosssa [chamada de atividades](call-for-proposals).
-->

## Patrocínio Ouro

  [link-collabora]: https://www.collabora.com.br
  [image-collabora]: /2018/images/logo-collabora.png "Collabora"

  [link-ae]: http://www.alhambra-eidos.com.br
  [image-ae]: /2018/images/logo-ae.png "Alhambra-Eidos"

[![Collabora][image-collabora]][link-collabora]
[![Alhambra-Eidos][image-ae]][link-ae]

## Patrocínio Prata

  [link-4linux]: http://www.4linux.com.br
  [image-4linux]: /2018/images/logo-4linux.png "4linux"

  [link-polilinux]: http://www.polilinux.com.br
  [image-polilinux]: /2018/images/logo-polilinux.png "PoliLinux"

[![4linux][image-4linux]][link-4linux]
[![PoliLinux][image-polilinux]][link-polilinux]

## Apoio

  [link-utfpr]: http://portal.utfpr.edu.br
  [image-utfpr]: /2018/images/logo-utfpr.png "UTFPR"

  [link-apufpr]: http://apufpr.org.br
  [image-apufpr]: /2018/images/logo-apufpr.png "Apufpr"

  [link-asl]: http://asl.org.br
  [image-asl]: /2018/images/logo-asl.png "ASL"

  [link-pslbrasil]: http://softwarelivre.org
  [image-pslbrasil]: /2018/images/logo-pslbrasil.png "PSL-Brasil"

  [link-coopfam]: http://www.coopfam.agr.br
  [image-coopfam]: /2018/images/logo-coopfam.png "COOPFAM"

  [link-cafefamiliar]: http://www.coopfam.agr.br
  [image-cafefamiliar]: /2018/images/logo-cafe-familiar.png "Café Familiar"

[![UTFPR][image-utfpr]][link-utfpr]
[![Apufpr][image-apufpr]][link-apufpr]
[![ASL][image-asl]][link-asl]
[![PSL-Brasil][image-pslbrasil]][link-pslbrasil]
[![COOPFAM][image-coopfam]][link-coopfam]
[![Café Familiar][image-cafefamiliar]][link-cafefamiliar]

## Organização

  [link-debianbrasil]: http://debianbrasil.org.br
  [image-debianbrasil]: /2018/images/logo-debianbrasil.png "Debian Brasil"

  [link-curitibalivre]: http://curitibalivre.org.br
  [image-curitibalivre]: /2018/images/logo-curitibalivre.png "Curitiba Livre"

[![Debian Brasil][image-debianbrasil]][link-debianbrasil]
[![Curitiba Livre][image-curitibalivre]][link-curitibalivre]

