---
title: Financiamento coletivo
---

# Financiamento coletivo

Você pode contribuir financeiramente com organização da MiniDebConf Curitiba
2018 fazendo uma doação de qualquer valor, mesmo que você não pretenda vir ao
evento. A doação será feita para a Comunidade Curitiba Livre que é o grupo
local que está organizando o evento.

O valor arrecadado será usado no pagamento de despesas como:

* Pessoal terceirizado que trabalhará no miniauditório no sábado;
* Confecção de material gráfico para divulgação;
* Confecção de banners de lona para usar na identidade visual;
* Coffee-breaks.

<br />
<div id="widgetwci">
  <div id="crm_wid_5" class="crm-wci-widget wide">
    <div class="crm-amount-bar">
      <div id="crm_wid_5_amt_hi" class="crm-amount-high">
        R$ 3.736,06
      </div>
      <div class="crm-amount-fill" id="crm_wid_5_amt_fill">
        <div id="crm_wid_5_low" class="crm-amount-low">
          R$ 3.940,00
        </div>
      </div>
    </div>
  </div>
</div>
<br />

<!-- INICIO FORMULARIO BOTAO PAGSEGURO -->
<form action="https://pagseguro.uol.com.br/checkout/v2/donation.html" method="post" target="_blank">
<fieldset>
<!-- NÃO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
<input type="hidden" name="currency" value="BRL" />
<input type="hidden" name="receiverEmail" value="daniel@lenharo.eti.br" />
<input type="hidden" name="iot" value="button" />
<input type="image" src="https://stc.pagseguro.uol.com.br/public/img/botoes/doacoes/209x48-doar-azul-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
</fieldset>
</form>
<!-- FINAL FORMULARIO BOTAO PAGSEGURO -->

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<fieldset>
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="brasil.mini@debconf.org">
<input type="hidden" name="lc" value="BR">
<input type="hidden" name="item_name" value="MiniDebConf Curitiba 2018">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="currency_code" value="BRL">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
<input type="image" src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - A maneira fácil e segura de enviar pagamentos online!">
<img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
</fieldset>
</form>

<fieldset>
![qrcode bitcoins](/images/qrcode.jpg "qrcode bitcoins")

Por favor, envie  BTC ao endereço Bitcoin 1AMrSpQQERi8f2HjAjZd4USY9tZCre6i2P.

bitcoin://1AMrSpQQERi8f2HjAjZd4USY9tZCre6i2P
</fieldset>

Se preferir fazer a **doação por depósito ou transferênca bancária**, mande um
e-mail para <brasil.mini@debconf.org> que te enviaremos os dados da conta.
Podemos receber pelos bancos: Caixa Econômica, Banco do Brasil ou Itaú.

Você também pode comprar produtos com a logomarca Debian no nossa loja on-line
no endereço: <http://loja.curitibalivre.org.br>. São camisetas, canecas de
porcelana, cordões para crachá e adesivos de vinil. Você poderá retirar os
produtos no local da MiniDebConf Curitiba 2018 de 11 a 14 de abril.

Se houver sobra de dinheiro, ele será usado para futuros eventos.

## Lista dos doadores

Vamos listar abaixo os nomes dos doadores (sem os valores) como forma de
reconhecimento pela ajuda.

<%= File.read items['/2018/donors.txt'][:filename] %>

## Observações:

* Se você deseja que a doação seja anômima e seu nome não apareça aqui no site,
basta enviar um email para <daniel@sombra.eti.br> solicitando.
* Se você fizer uma doação em bitcoins, nos mande um email para
<daniel@sombra.eti.br> avisando para que possamos colocar seu nome aqui.

